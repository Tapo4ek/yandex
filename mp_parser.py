# -*- coding: utf-8 -*-


import os
import sys
from multiprocessing import forking, Process, Queue, cpu_count
from mixins import ParserMixin
from settings import BASE_DIR
from xmlrpc import XmlRpcServer, XmlRpcClient


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class _Popen(forking.Popen):
    def __init__(self, *args, **kw):
        if hasattr(sys, 'frozen'):
            # We have to set original _MEIPASS2 value from sys._MEIPASS
            # to get --onefile mode working.
            # Last character is stripped in C-loader. We have to add
            # '/' or '\\' at the end.
            os.putenv('_MEIPASS2', sys._MEIPASS + os.sep)
        try:
            super(_Popen, self).__init__(*args, **kw)
        finally:
            if hasattr(sys, 'frozen'):
                # On some platforms (e.g. AIX) 'os.unsetenv()' is not
                # available. In those cases we cannot delete the variable
                # but only set it to the empty string. The bootloader
                # can handle this case.
                if hasattr(os, 'unsetenv'):
                    os.unsetenv('_MEIPASS2')
                else:
                    os.putenv('_MEIPASS2', '')


class ParsingProcess(ParserMixin, Process):
    """
    Класс, описывающий процесс парсинга одной записи переданной в MpParser
    """
    _Popen = _Popen

    def __init__(self, filename, counter_class, pid, result_queue):
        """
        :param pid: уникальный идентификатор процесса равный его порядковому номеру при запуске
        :type pid: int
        :param result_queue: очередь для передачи сообщений предку
        :type result_queue: Queue
        :param filename: путь до файла
        :type filename: str
        """
        super(ParsingProcess, self).__init__()
        self.process_pid = pid
        self.filename = filename
        self.result_queue = result_queue
        self.counter = counter_class()

    def run(self):
        """
        Запуск процесса
        Формирует результат парсинга в очередь предка в формате (pid, raw_file_record, result).
        Коды ошибок result: 0 --- ошибок нет, 1 --- возникла ошибка.
        """
        result = 0
        print '*' * 80
        print 'parsing process {0} has satrted on file {1}'.format(self.process_pid, os.path.basename(self.filename))
        try:
            self.parse()
            result = 0
        except Exception as e:
            import traceback
            print traceback.format_exc()
            print '*' * 80
            result = 1
        print 'parsing process {0} has finished with result = {1}'.format(self.process_pid, result)
        self.result_queue.put((self.process_pid, result))


class MpParser(object):
    """
    Класс реализующий логику мультипроцессного парсинга
    """
    def __init__(self, dirpath, output_path=None):

        self.files = map(lambda x: os.path.join(dirpath, x), [i for i in os.listdir(dirpath)])
        self.result_queue = Queue()
        self.server = XmlRpcServer()
        self.output_path = output_path
        if self.output_path is None:
            self.output_path = os.path.join(BASE_DIR, 'output.txt')

    def start(self):
        """
        Попытка оптимизировать по процессорам, вычисляю количество ядер процессора на компьютере
        и запускаю по процессу на файл, в случае если файлов больше, то последующие файлы
        перед запуском будут ожидать окончания работы предыдущих процессов
        """

        # Запуск Xmlpc сервера в который будут плевать данные все процессы
        self.server.start()
        file_parsers = list()
        cores = cpu_count()
        process_count = 0
        result = dict()
        file_records_dict = dict()
        while len(self.files):
            cpu_parsers = 0
            for cpu in range(0, cores):
                if not len(self.files):
                    break
                cpu_parsers += 1
                rfr = self.files.pop()
                process_count += 1
                file_records_dict[process_count] = rfr
                file_parsers.append(
                    ParsingProcess(
                        rfr,
                        XmlRpcClient,  # Передаю класс счетчика
                        process_count,
                        self.result_queue,
                    )
                )
            for rfp in file_parsers[-cpu_parsers:]:
                rfp.start()

            for rfp in file_parsers[-cpu_parsers:]:
                rfp.join()

            while not self.result_queue.empty():
                # формат ответ: (0: идентификатор процесса, 1: код ошибки)
                pid, err = self.result_queue.get()
                if err not in result.keys():
                    result[err] = list()
                result[err].append((pid, file_records_dict[pid]))
                print '=' * 80
                print 'pid = %s' % pid
        return result

    def create_output_file(self):
        self.server.server.instance.create_output_file(self.output_path)
        self.server.stop()

