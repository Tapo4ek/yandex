# -*- coding: utf-8 -*-


import os
import sqlite3
import json
from collections import defaultdict
from settings import BASE_DIR


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class UniqueIdGenerator(object):
    """
    Класс генерирующий уникальные идентификаторы
    """
    def __init__(self):
        self.ids = defaultdict(int)

    def generate_id(self, key):
        self.ids[key] += 1
        return self.ids[key]


class DbAdapter(object):
    """
    Класс определяющий API работы с базой данных
    """
    def __init__(self, filename):
        self.filename = filename
        self.cursor = None
        self.is_connected = False
        self.connection = None
        self.id_generator = UniqueIdGenerator()

    def connect(self):
        self.connection = sqlite3.connect(self.filename)
        self.cursor = self.connection.cursor()
        self.is_connected = True

    def commit(self):
        self.connection.commit()

    def create(self, tablename, fields):
        self.execute('CREATE TABLE "{0}" ({1});'.format(tablename, self.generate_fields(fields)))

    def insert(self, tablename, fields, commit=False):
        values = list()
        keys = list()
        for key, value in fields.items():
            keys.append('{0}'.format(key))
            values.append('"{0}"'.format(value))
        sql = 'INSERT INTO {0} ({1}) VALUES ({2});'.format(tablename, ','.join(keys), ','.join(values))
        self.cursor.execute(sql)
        if commit:
            self.connection.commit()

    def update(self, tablename, fields, where, commit=False):
        condition = self.create_condition(',', fields)
        where = self.create_condition(',', where)
        sql = 'UPDATE {0} SET {1} WHERE {2};'.format(tablename, condition, where)
        self.cursor.execute(sql)
        if commit:
            self.connection.commit()

    def select(self, fields, tablename, where=None):
        sql = 'SELECT {0} FROM {1} '.format(','.join(fields), tablename)
        if where:
            self.select_where(sql, where)
        return self.execute(sql + ';')

    def select_where(self, sql, fields):
        strings = list()
        for key, value in fields.iteritems():
            strings.append('{0}="{1}"'.format(key, value))
        where = ' AND '.join(strings)
        return self.execute('{0} WHERE {1};'.format(sql, where))

    def create_condition(self, joiner, fields_dict):
        joiner = ' {0} '.format(joiner)
        strings = list()
        for key, value in fields_dict.iteritems():
            strings.append('{0}="{1}"'.format(key, value))
        return joiner.join(strings)

    def generate_fields(self, fields_dict):
        strings = list()
        for key, value in fields_dict.items():
            strings.append('"{0}" {1}'.format(key, value))
        return ', '.join(strings)

    def execute(self, sql, update=False):
        if not self.is_connected:
            self.connect()
        response = self.cursor.execute(sql)
        if update:
            self.connection.commit()
        return response.fetchall()

    def execute_list(self, sql_list):
        if not self.is_connected:
            self.connect()
        for sql in sql_list:
            self.cursor.execute(sql)
        self.connection.commit()

    def disconnect(self):
        self.is_connected = False
        self.connection.close()

    def remove_file(self):
        os.remove(self.filename)


class YaDbAdapter(DbAdapter):
    """
    Класс для работы непосредственно с нужными таблицами
    """
    def __init__(self, filename):
        super(YaDbAdapter, self).__init__(filename)
        self.id_generator = UniqueIdGenerator()

    def create_request_table(self):
        table_fields = {
            'request_id': 'integer NOT NULL PRIMARY KEY',  # Идентификатор запроса
            'user_send_fase': 'integer NOT NULL',
            "request_time": 'integer NOT NULL',
            'not_full_answer_list': 'text',
            'all_errors': 'integer',
            'url_list': 'text',
            'errors_dict': 'text'
        }
        self.create('request', table_fields)

    def create_tables(self):
        self.create_request_table()

    def flush_request(self, request_list):
        for request_id, user_send_fase, request_time, not_full_answer_list, all_errors, url_list, errors_dict in request_list:
            sql = """INSERT INTO request (
                    request_id,
                    user_send_fase,
                    request_time,
                    not_full_answer_list,
                    all_errors,
                    url_list,
                    errors_dict) VALUES (?, ?, ?, ?, ?, ?, ?);"""
            self.cursor.execute(sql, (
                request_id,
                user_send_fase,
                request_time,
                not_full_answer_list,
                all_errors,
                url_list,
                errors_dict
            ))
        self.commit()

    def get_percentil(self, percentil):
        length = self.execute('SELECT count(*) FROM request;')[0][0]
        el_index = round(length * percentil / 100.0) - 1
        return self.execute('SELECT request_id, request_time FROM request ORDER BY request_time LIMIT 1 OFFSET {0};'.format(el_index))

    def get_the_longest_request_ids(self):
        return self.execute('SELECT request_id, user_send_fase FROM request ORDER BY user_send_fase DESC LIMIT 10 OFFSET 0;')

    def get_not_full_answer_requests(self):
        return self.execute('SELECT request_id, not_full_answer_list FROM request WHERE not_full_answer_list is not NULL;')


if __name__ == '__main__':
    adapter = YaDbAdapter(os.path.join(BASE_DIR, 'test.db'))
    if not adapter.table_exists:
        adapter.create_tables()
    adapter.disconnect()