# -*- coding: utf-8 -*-


import json
from collections import defaultdict


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class BackendErrorCounterStructure(object):

    def __init__(self):

        # В случае, если запросы с разными идентификаторами отправлялись на этот адрес
        self.request_id = defaultdict(int)
        self.rg_ids = defaultdict(int)
        self.error_counter_dict = defaultdict(int)

        #  Счетчики
        self._errors_counter = 0
        self._requests_counter = 0

    def request(self, data):
        """

        :param data: (request_id, rg_id)
        :type data: tuple
        :return:
        """
        self.add_request()
        self.rg_ids[data[1]] += 1
        self.request_id[data[0]] += 1

    def error(self, error_text):
        """
        :param error_text: текст возникшей ошибки
        :type error_text: str
        :return:
        """
        self.add_error()
        self.error_counter_dict[error_text] += 1

    def add_error(self):
        self._errors_counter += 1
        return self._errors_counter

    def add_request(self):
        self._requests_counter += 1
        return self._requests_counter

    def get_errors(self):
        return self._errors_counter

    def get_requests(self):
        return self._requests_counter


class RequestStructure(object):

    def __init__(self):

        # Идентификатор запроса. Уникален для класса
        self.request_id = None

        # Тайминги
        self._start_request = None
        self._finish_request = None
        self._start_send_result = None

        # счетчики
        self._backend_request_counter = 0
        self._backend_error_counter = 0
        self._backend_ok_counter = 0

        # Сет подсчета BackendRequest не получивших BackendOk
        self._backend_request_set = set()

        # Словари подсчета групп реплик
        self._rg_url_dict = defaultdict(int)
        self._rg_error_dict = dict()

        # словарик для получения урла по которому произошла ошибка
        self._rg_id_rg_url = dict()

    def start_request(self, timestamp, request_id):
        self._start_request = timestamp
        self.request_id = request_id

    def finish_request(self, timestamp):
        self._finish_request = timestamp

    def start_send_result(self, timestamp):
        self._start_send_result = timestamp

    def backend_request(self, rg_id):
        self._backend_request_counter += 1
        self._backend_request_set.add(rg_id)

    def backend_connect(self, rg_id, url):
        self._rg_id_rg_url[rg_id] = url
        self._rg_url_dict[url] += 1

    def backend_error(self, rg_id, error_text):
        self._backend_error_counter += 1
        url = self._rg_id_rg_url[rg_id]
        errors_dict = self._rg_error_dict.get(url, defaultdict(int))
        errors_dict[error_text] += 1
        self._rg_error_dict[url] = errors_dict

    def backend_ok(self, rg_id):
        self._backend_ok_counter += 1
        self._backend_request_set.discard(rg_id)

    def get_user_send_fase(self):
        return self._finish_request - self._start_send_result

    def get_request_time(self):
        return self._finish_request - self._start_request

    def get_not_full_answers(self):
        return self._backend_request_set

    def get_all_errors(self):
        return self._backend_error_counter

    def get_rg_errors(self):
        return self._to_json(self._rg_error_dict)

    def get_rg_urls(self):
        return self._to_json(self._rg_url_dict)

    def _to_json(self, data):
        return json.dumps(data)