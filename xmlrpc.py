# -*- coding: utf-8 -*-


import xmlrpclib
import httplib
import threading
import operator
import codecs
from collections import defaultdict
from threading import Lock
from structures import BackendErrorCounterStructure
from SimpleXMLRPCServer import SimpleXMLRPCRequestHandler, SimpleXMLRPCServer
from utils import convert_timestamp
from settings import SERVER_ADDRESS, SERVER_PORT


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class ServerRemoteMethods(object):
    def __init__(self):
        self.lock = Lock()

        # Словари тайминга
        self.start_finish_timing = dict()
        self.start_send_result_finish_timing = dict()

        # Рабочие словари для подсчета данных
        self.start_finish_request = dict()
        self.start_send_result_finish_request = dict()
        self._backend_connect_dict = defaultdict(BackendErrorCounterStructure)
        self.backend_request_set = set()

        # Словарь для последующего определения по какому адресу возникла ошибка
        self._req_id_rg_id_request_url = dict()

    def start_request(self, date, key):
        print 'start {0}'.format(key)
        date = convert_timestamp(date)
        self.start_finish_timing[key] = date
        return 0

    def start_send_result(self, date, key):
        date = convert_timestamp(date)
        self.start_send_result_finish_timing[key] = date
        return 0

    def finish_request(self, date, key):
        print 'finish {0}'.format(key)
        date = convert_timestamp(date)
        start_request_time = self.start_finish_timing[key]
        start_send_result_time = self.start_send_result_finish_timing[key]
        self.start_finish_request[key] = date - start_request_time
        self.start_send_result_finish_request[key] = date - start_send_result_time
        return 0

    def backend_error(self, request_id, rg_id, error_text):
        backend_url = self._req_id_rg_id_request_url[(request_id, rg_id)]
        self._backend_connect_dict[backend_url].error(error_text)
        return 0

    def backend_connect(self, request_id, rg_id, url):
        backend_url = url.split('/')[2]
        self._req_id_rg_id_request_url[(request_id, rg_id)] = backend_url
        self._backend_connect_dict[backend_url].request((request_id, rg_id))
        return 0

    def backend_ok(self, request_id, rg_id):
        self.backend_request_set.discard((request_id, rg_id))
        return 0

    def backend_request(self, request_id, rg_id):
        self.backend_request_set.add((request_id, rg_id))
        return 0

    def get_the_longest_request_ids(self, amount=10):
        """
        Тут можно было бы использовать OrderedDict и при добавлении элемента вставлять его сразу в нужное место
        Но для того чтобы понять куда вставить нужно найти это место. Алгоритм быстрее чем log(n) мне в голову не пришел
        Так как функция sorted тоже имеет логарифмический рост: оставил так.
        """
        res = sorted(self.start_send_result_finish_request.items(), key=operator.itemgetter(1), reverse=True)[:amount]
        return map(lambda x: (x[0], str(x[1])), res)

    def get_percentil(self, percentil=95):
        length = len(self.start_finish_request)
        sorted_by_timing = sorted(self.start_finish_request.items(), key=operator.itemgetter(1))
        el_index = round(length * percentil / 100.0) - 1
        result = sorted_by_timing[int(el_index)]
        return result[0], str(result[1])

    def get_no_full_answer_requests(self):
        """
        Возвращаю словарь с ключами
        идентификаторами запросов с неполным ответом и значениями
        списками не ответивших репликационных групп
        """
        result = defaultdict(list)
        for record in self.backend_request_set:
            result[record[0]].append(record[1])
        return dict(result)

    def get_backend_connect_dict(self):
        # return jsonpickle.encode(self._backend_connect_dict)
        pass
    def create_output_file(self, filepath):
        with codecs.open(filepath, 'w', 'utf-8') as ostream:
            percentil = self.get_percentil()[1]
            ostream.write(u'95-й перцентиль времени работы: {0}\n\n'.format(percentil))

            ostream.write(u'Идентификаторы запросов с самой долгой фазой отправки результатов пользователю:\n\n')
            for i in self.get_the_longest_request_ids():
                ostream.write(u'\t{0}\t:\t{1} \n'.format(i[0], i[1]))

            ostream.write(u'\nИдентификаторы запросов с неполным набором ответивших РГ:\n\n')
            nfar_dict = self.get_no_full_answer_requests()
            for nfar in nfar_dict:
                ostream.write(u'\tИдентификатор={0}:'.format(nfar))
                ostream.write(u'\t не ответившие ГР:')
                ostream.write('\t{0}\n'.format(','.join(nfar_dict[nfar])))

            ostream.write(u'\n\nОбращения и ошибки по бекендам:\n')
            # backend_connect_dict = jsonpickle.decode(self.get_backend_connect_dict())
            pass
            for key in backend_connect_dict:
                ostream.write(u'\t{0}\n'.format(key))
                ostream.write(u'\t\tОбращения: {0}\n'.format(backend_connect_dict[key].get_requests()))
                ostream.write(u'\t\tГР:\n')
                for k, v in backend_connect_dict[key].rg_ids.items():
                    times_string = u'раз'
                    if str(v)[-1] in '234':
                        times_string = u'раза'
                    ostream.write(u'\t\t\t{0} \t\t: {1} {2}\n'.format(k, v, times_string))
                #  Решил идентификаторы запроса со фронтенда уж не вписывать
                ostream.write(u'\t\tОшибки: {0}\n'.format(backend_connect_dict[key].get_errors()))
                for k, v in backend_connect_dict[key].error_counter_dict.items():
                    times_string = u'раз'
                    if str(v)[-1] in '234':
                        times_string = u'раза'
                    ostream.write(u'\t\t\t{0} \t\t: {1} {2}\n'.format(k, v, times_string))
        return 0


class XmlRpcServer(threading.Thread):
    """
    Класс запускающий сервер хранения данных.
    """
    def __init__(self):
        threading.Thread.__init__(self)
        self.server = SimpleXMLRPCServer(
            addr=(SERVER_ADDRESS, SERVER_PORT),
            requestHandler=SimpleXMLRPCRequestHandler,
            logRequests=False
        )
        self.server.register_instance(ServerRemoteMethods())

    def stop(self):
        """
        Остановка сервера.
        """
        self.server.shutdown()

    def run(self):
        """
        Запуск транспортного потока.
        """
        self.server.serve_forever()


class TimeoutHTTPConnection(httplib.HTTPConnection):

    def __init__(self, host, timeout=10):
        httplib.HTTPConnection.__init__(self, host, timeout=timeout)


class TimeoutHTTP(httplib.HTTP):
    _connection_class = TimeoutHTTPConnection

    def set_timeout(self, timeout):
        self._conn.timeout = timeout


class TimeoutTransport(xmlrpclib.Transport):
    def __init__(self, timeout=10, *l, **kw):
        xmlrpclib.Transport.__init__(self, *l, **kw)
        self.timeout = timeout

    def make_connection(self, host):
        conn = TimeoutHTTPConnection(host, self.timeout)
        return conn


class TimeoutServerProxy(xmlrpclib.ServerProxy):
    def __init__(self, uri, timeout=10, *l, **kw):
        kw['transport'] = TimeoutTransport(timeout=timeout, use_datetime=kw.get('use_datetime', 0))
        xmlrpclib.ServerProxy.__init__(self, uri, *l, **kw)


class XmlRpcClient(object):

    def __init__(self):
        self.server = TimeoutServerProxy('http://{0}:{1}'.format(SERVER_ADDRESS, SERVER_PORT), timeout=2)

    def start_request(self, date, key):
        self.server.start_request(date, key)

    def start_send_result(self, date, key):
        self.server.start_send_result(date, key)

    def finish_request(self, date, key):
        self.server.finish_request(date, key)

    def backend_error(self, request_id, rg_id, error_text):
        self.server.backend_error(request_id, rg_id, error_text)

    def backend_connect(self, request_id, rg_id, url):
        self.server.backend_connect(request_id, rg_id, url)

    def backend_ok(self, request_id, rg_id):
        self.server.backend_ok(request_id, rg_id)

    def backend_request(self, request_id, rg_id):
        self.server.backend_request(request_id, rg_id)

    def get_percentil(self, percentil=95):
        return self.server.get_percentil(percentil)

    def get_no_full_answer_requests(self):
        return self.server.get_no_full_answer_requests()

    def get_the_longest_request_ids(self, amount=10):
        return self.server.get_the_longest_request_ids(amount)

    def get_backend_connect_dict(self):
        return self.server.get_backend_connect_dict()


if __name__ == '__main__':
    server = XmlRpcServer()
    server.start()
    client = XmlRpcClient()
    server.stop()