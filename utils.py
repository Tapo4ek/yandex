# -*- coding: utf-8 -*-


import datetime


__author__ = 'Tapo4ek <infdods@yandex.ru>'


def convert_timestamp(timestamp):
    return datetime.datetime.fromtimestamp(float(timestamp) / 1e3 / 1e3)