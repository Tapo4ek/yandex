# -*- coding: utf-8 -*-

import os
import sys
import codecs
import json
import tempfile
from optparse import OptionParser
from adapter import YaDbAdapter
from structures import RequestStructure
from settings import BASE_DIR, DB_FLUSH_SIZE

TMP_DIR = tempfile.gettempdir()


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class RequestCounter(object):
    def __init__(self):
        self.request_data_list = list()
        self.db_adapter = YaDbAdapter(os.path.join(BASE_DIR, 'db.db'))
        self.db_adapter.create_tables()

    def full_request(self, request_structure):

        request_id = request_structure.request_id
        user_send_fase = request_structure.get_user_send_fase()
        request_time = request_structure.get_request_time()
        not_full_answers = request_structure.get_not_full_answers()
        not_full_answers = ','.join(not_full_answers) if not_full_answers else None
        all_errors = request_structure.get_all_errors()
        url_list = request_structure.get_rg_urls()
        errors_dict = request_structure.get_rg_errors()

        self.request_data_list.append((
            request_id,
            user_send_fase,
            request_time,
            not_full_answers,
            all_errors,
            url_list,
            errors_dict
        ))
        if len(self.request_data_list) > DB_FLUSH_SIZE:
            self.flush()

    def file_finished(self):
        """
        Получить все данные из базы
        """
        self.flush()

    def flush(self):
        self.db_adapter.flush_request(self.request_data_list)
        self.request_data_list = list()

    def get_percentil(self, percentil=95):
        return self.db_adapter.get_percentil(percentil=percentil)

    def get_the_longest_request_ids(self):
        return self.db_adapter.get_the_longest_request_ids()


class Parser(object):
    """
    Класс, описывающий процесс парсинга одного файла
    """

    def __init__(self, filename, output_path=None):
        self.filename = filename
        self.output_path = output_path
        self.counter = RequestCounter()
        self.request_structire = RequestStructure()

    def parse(self):

        with open(self.filename, 'r') as istream:
            line = istream.readline()
            request_id = None
            while line:
                line = line.strip().split('\t')
                if request_id != line[1] and self.request_structire.request_id is not None:
                    self.counter.full_request(self.request_structire)
                    self.request_structire = RequestStructure()

                #  Только timestamp, request_id и action_name
                if len(line) == 3:
                    #  Начало запроса
                    if line[2] == 'StartRequest':
                        self.request_structire.start_request(int(line[0]), line[1])

                    #  Конец мерджа и начало отправки пользователю
                    elif line[2] == 'StartSendResult':
                        self.request_structire.start_send_result(int(line[0]))

                    #  Конец запроса
                    elif line[2] == 'FinishRequest':
                        self.request_structire.finish_request(int(line[0]))

                #  timestamp, request_id, action_name и rg_id
                elif len(line) == 4:

                    #  Отправка запроса на бекенд
                    if line[2] == 'BackendRequest':
                        self.request_structire.backend_request(line[3])

                    #  Ответ от бекенда успешен
                    elif line[2] == 'BackendOk':
                        self.request_structire.backend_ok(line[3])

                # timestamp, request_id, action_id, rg_id, error_text | url
                elif len(line) == 5:

                    #  Ошибка связи с бекендом
                    if line[2] == 'BackendError':
                        self.request_structire.backend_error(line[3], line[4])

                    #  Отправка запроса на бекенд
                    if line[2] == 'BackendConnect':
                        self.request_structire.backend_connect(line[3], line[4].split('/')[2])

                request_id = line[1]
                line = istream.readline()

            self.counter.full_request(self.request_structire)
            self.request_structire = RequestStructure()
            self.counter.file_finished()

    def create_output_file(self, filepath=None):
        if filepath is None:
            filepath = os.path.join(BASE_DIR, 'output.txt')
        with codecs.open(filepath, 'w', 'utf-8') as ostream:
            percentil = self.counter.get_percentil()[0][1]
            ostream.write(u'95-й перцентиль времени работы: {0}\n\n'.format(percentil))

            ostream.write(u'Идентификаторы запросов с самой долгой фазой отправки результатов пользователю:\n\n')
            for i in self.counter.get_the_longest_request_ids():
                ostream.write(u'\t{0}\t:\t{1} \n'.format(i[0], i[1]))

            ostream.write(u'\nИдентификаторы запросов с неполным набором ответивших РГ:\n\n')
            nfar_list = self.counter.db_adapter.get_not_full_answer_requests()
            for nfar in nfar_list:
                ostream.write(u'\tИдентификатор={0}:'.format(nfar[0]))
                ostream.write(u'\t не ответившие ГР:')
                ostream.write('\t{0}\n'.format(nfar[1]))
            ostream.write('\n\n')

            sql = "SELECT request_id, all_errors, url_list, errors_dict  FROM request LIMIT 1 OFFSET %s;"
            counter = 0
            record = self.counter.db_adapter.execute(sql % counter)
            while record:
                record = record[0]
                url_dict = json.loads(record[2])
                errors_dict = json.loads(record[3])
                ostream.write(u'\nОбращения и ошибки по бекенду {0}:\n'.format(record[0]))
                for key, value in url_dict.iteritems():
                    ostream.write(u'\t {0}\n'.format(key))
                    ostream.write(u'\t\t Обращения {0}\n'.format(value))
                    errors = errors_dict.get(key)
                    if errors:
                        ostream.write(u'\t\t Ошибки: \n')
                        for error_name, error_amount in errors.iteritems():
                            ostream.write(u'\t\t\t {0}: {1}\n'.format(error_name, error_amount))

                counter += 1
                record = self.counter.db_adapter.execute(sql % counter)


def usage():
    print 'usage: [--file file] [-f file]\n'


def main(args):
    opt_parser = OptionParser()
    opt_parser.add_option("-f", "--file", dest="filepath")
    (options, args) = opt_parser.parse_args()
    if not options.filepath:
        usage()
        sys.exit(2)

    # создадим отсортированную копию файла в темповой директории
    filename = os.path.basename(options.filepath)
    os.system('sort -k2 -n {0} > {1}'.format(options.filepath, os.path.join(TMP_DIR, filename)))
    parser = Parser(os.path.join(TMP_DIR, filename))
    parser.parse()
    # почистим за собой темповую папку
    os.remove(os.path.join(TMP_DIR, filename))
    parser.create_output_file(os.path.join(BASE_DIR, 'output.txt'))
    parser.counter.db_adapter.remove_file()

if __name__ == "__main__":
    main(sys.argv[1:])
