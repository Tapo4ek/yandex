# -*- coding: utf-8 -*-


__author__ = 'Tapo4ek <infdods@yandex.ru>'

from structures import RequestStructure

class ParserMixin(object):
    """
    Миксин, расширяющий класс и добавляющий ему метод parse
    """

    def parse(self):

        with open(self.filename, 'r') as istream:
            line = istream.readline()
            lines = list()
            request_id = None
            while line:
                line = line.strip().split('\t')
                if request_id != line[1]:
                    self.counter.full_request()


                #  Только timestamp, request_id и action_name
                if len(line) == 3:
                    #  Начало запроса
                    if line[2] == 'StartRequest':
                        # date = Utils.convert_timestamp(line[0])
                        self.counter.start_request(line[0], line[1])

                    #  Конец мерджа и начало отправки пользователю
                    elif line[2] == 'StartSendResult':
                        self.counter.start_send_result(line[0], line[1])

                    #  Конец запроса
                    elif line[2] == 'FinishRequest':
                        self.counter.finish_request(line[0], line[1])

                #  timestamp, request_id, action_name и rg_id
                elif len(line) == 4:

                    #  Отправка запроса на бекенд
                    if line[2] == 'BackendRequest':
                        self.counter.backend_request(line[1], line[3])

                    #  Ответ от бекенда успешен
                    elif line[2] == 'BackendOk':
                        self.counter.backend_ok(line[1], line[3])

                # timestamp, request_id, action_id, rg_id, error_text | url
                elif len(line) == 5:

                    #  Ошибка связи с бекендом
                    if line[2] == 'BackendError':
                        self.counter.backend_error(line[0], line[1], line[3], line[4])

                    #  Отправка запроса на бекенд
                    if line[2] == 'BackendConnect':
                        self.counter.backend_connect(line[0], line[1], line[3], line[4])

                line = istream.readline()
            self.file_finished()


class SortedParserMixin(object):
    """
    Миксин, расширяющий класс и добавляющий ему метод parse
    """

    def parse(self):
        with open(self.filename, 'r') as istream:
            line = istream.readline()
            lines = list()
            request_id = None
            while line:
                line = line.strip().split('\t')
                if request_id != line[1] and len(lines):
                    self.counter.full_request(lines)
                    lines = list()
                else:
                    lines.append(line)

                request_id = line[1]
                line = istream.readline()

            # Последний запрос
            if len(lines):
                self.counter.full_request(lines)
            self.file_finished()