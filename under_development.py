# -*- coding: utf-8 -*-


import os
import sys
from optparse import OptionParser
from parser import Parser
from mp_parser import MpParser
from settings import BASE_DIR


__author__ = 'Tapo4ek <infdods@yandex.ru>'


def usage():
    print 'usage: [--file file] [-f file]\n'


def main(args):
    opt_parser = OptionParser()
    opt_parser.add_option("-f", "--file", dest="filepath")
    opt_parser.add_option("-m", "--multiprocess", dest="multiprocess", action="store_true")
    opt_parser.add_option("-d", "--directory", dest="directory")
    opt_parser.add_option("-o", "--output", dest="output")
    (options, args) = opt_parser.parse_args()

    # Не передан путь до output файла
    if not options.output:
        options.output = os.path.join(BASE_DIR, 'output.txt')

    # Подразумавается что программа запускается в мультипроцессинге
    if not options.filepath:
        if not options.multiprocess and options.directory:
            usage()
            sys.exit(2)
        else:
            parser_class = MpParser(options.directory, output_path=options.output)
    # Программе передан путь к файлу, значит парсер запускается только на 1 файл
    else:
        parser_class = Parser(options.filepath, output_path=options.output)
    parser_class.start()
    parser_class.create_output_file()


if __name__ == "__main__":
    main(sys.argv[1:])
