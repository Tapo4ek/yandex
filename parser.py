# -*- coding: utf-8 -*-


import os
import sys
import codecs
import time
from optparse import OptionParser
from collections import defaultdict
from memory_profiler import profile
from mixins import ParserMixin
from adapter import YaDbAdapter
from settings import BASE_DIR, DB_FLUSH_SIZE


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class YaCounter(object):
    def __init__(self):

        self._start_send_result = dict()
        self._start_request = dict()
        self._finish_request = dict()

        self._backend_connect_dict = dict()
        self.backend_request_set = set()

        self._backend_connect_list = list()
        self._backend_error_list = list()

        self.db_adapter = YaDbAdapter(os.path.join(BASE_DIR, 'db.db'))

    def file_finished(self):
        """
        Файл закончен и надо закинуть в базу те словари что успели набраться, но не достигли нужной длины
        """
        self._flush_start_request()
        self._flush_finish_request()
        self._flush_start_send_result()
        self._flush_backend_connect_list()
        self._flush_backend_error_list()

    def _flush_backend_connect_list(self):
        self.db_adapter.flush_backend_connect_list(self._backend_connect_list)
        self._backend_connect_list = list()

    def _flush_backend_error_list(self):
        self.db_adapter.flush_backend_error_list(self._backend_error_list)
        self._backend_error_list = list()

    def _flush_start_send_result(self):
        self.db_adapter.flush_start_send_result(self._start_send_result)
        self._start_send_result = dict()

    def _flush_start_request(self):
        self.db_adapter.flush_start_request(self._start_request)
        self._start_request = dict()

    def _flush_finish_request(self):
        self.db_adapter.flush_finish_request(self._finish_request)
        self._finish_request = dict()

    def start_request(self, date, key):
        self._start_request[key] = int(date)
        if len(self._start_request) > DB_FLUSH_SIZE:
            self._flush_start_request()

    def start_send_result(self, date, key):
        self._start_send_result[key] = int(date)
        if len(self._start_send_result) > DB_FLUSH_SIZE:
            self._flush_start_send_result()

    def finish_request(self, date, key):
        self._finish_request[key] = int(date)
        if len(self._finish_request) > DB_FLUSH_SIZE:
            self._flush_start_request()
            self._flush_start_send_result()
            self._flush_finish_request()

    def backend_error(self, timestamp, request_id, rg_id, error_text):
        self._backend_error_list.append((int(timestamp), request_id, rg_id, error_text))
        if len(self._backend_error_list) > DB_FLUSH_SIZE:
            # Если есть запросы находящиеся в этом списке
            self._flush_backend_connect_list()
            self._flush_backend_error_list()

    def backend_connect(self, date, request_id, rg_id, url):
        backend_url = url.split('/')[2]
        self._backend_connect_list.append((int(date), request_id, rg_id, backend_url))
        if len(self._backend_connect_list) > DB_FLUSH_SIZE:
            self._flush_backend_connect_list()

    def backend_ok(self, request_id, rg_id):
        self.backend_request_set.discard((request_id, rg_id))

    def backend_request(self, request_id, rg_id):
        self.backend_request_set.add((request_id, rg_id))

    def get_no_full_answer_requests(self):
        """
        Возвращаю словарь с ключами
        идентификаторами запросов с неполным ответом и значениями
        списками не ответивших репликационных групп
        """
        result = defaultdict(list)
        for record in self.backend_request_set:
            result[record[0]].append(record[1])
        return result

    def get_backend_connect_dict(self):
        return self._backend_connect_dict

    def db_post_work(self):
        self.db_adapter.db_post_work()

    def create_output_file(self, filepath=None):
        if filepath is None:
            filepath = os.path.join(BASE_DIR, 'output.txt')
        with codecs.open(filepath, 'w', 'utf-8') as ostream:
            percentil = self.get_percentil()[1]
            ostream.write(u'95-й перцентиль времени работы: {0}\n\n'.format(percentil))

            ostream.write(u'Идентификаторы запросов с самой долгой фазой отправки результатов пользователю:\n\n')
            for i in self.get_the_longest_request_ids():
                ostream.write(u'\t{0}\t:\t{1} \n'.format(i[0], i[1]))

            ostream.write(u'\nИдентификаторы запросов с неполным набором ответивших РГ:\n\n')
            nfar_dict = self.get_no_full_answer_requests()
            for nfar in nfar_dict:
                ostream.write(u'\tИдентификатор={0}:'.format(nfar))
                ostream.write(u'\t не ответившие ГР:')
                ostream.write('\t{0}\n'.format(','.join(nfar_dict[nfar])))

            ostream.write(u'\n\nОбращения и ошибки по бекендам:\n')
            backend_connect_dict = self.get_backend_connect_dict()
            for key in backend_connect_dict:
                ostream.write(u'\t{0}\n'.format(key))
                ostream.write(u'\t\tОбращения: {0}\n'.format(backend_connect_dict[key].get_requests()))
                ostream.write(u'\t\tГР:\n')
                for k, v in backend_connect_dict[key].rg_ids.items():
                    times_string = u'раз'
                    if str(v)[-1] in '234':
                        times_string = u'раза'
                    ostream.write(u'\t\t\t{0} \t\t: {1} {2}\n'.format(k, v, times_string))
                #  Решил идентификаторы запроса со фронтенда уж не вписывать
                ostream.write(u'\t\tОшибки: {0}\n'.format(backend_connect_dict[key].get_errors()))
                for k, v in backend_connect_dict[key].error_counter_dict.items():
                    times_string = u'раз'
                    if str(v)[-1] in '234':
                        times_string = u'раза'
                    ostream.write(u'\t\t\t{0} \t\t: {1} {2}\n'.format(k, v, times_string))


class Parser(ParserMixin):
    """
    Класс, описывающий процесс парсинга одного файла
    """

    def __init__(self, filename, output_path=None):
        self.filename = filename
        self.counter = YaCounter()
        self.output_path = output_path
        if self.output_path is None:
            os.path.join(BASE_DIR, 'output.txt')

    def create_output_file(self):
        self.counter.create_output_file(self.output_path)

    def start(self):
        """
        Сделано для единого интерфейса парсинга с мультипроцессным вариантом
        """
        self.parse()

    def file_finished(self):
        self.counter.file_finished()

    def db_post_work(self):
        self.counter.db_post_work()


def usage():
    print 'usage: [--file file] [-f file]\n'

@profile
def main(args):
    opt_parser = OptionParser()
    opt_parser.add_option("-f", "--file", dest="filepath")
    (options, args) = opt_parser.parse_args()
    if not options.filepath:
        usage()
        sys.exit(2)
    start_time = time.time()
    parser = Parser(options.filepath)
    parser.counter.db_adapter.remove_file()
    parser.counter.db_adapter.create_tables()
    parser.start()
    parser.db_post_work()
    print '*' * 80
    print parser.counter.db_adapter.get_the_longest_request_ids()
    print parser.counter.db_adapter.get_percentil()
    end_time = time.time()
    print u'Время выполнения = {0}'.format(end_time - start_time)
    # print parser
    # print parser.counter.db_adapter.get_the_longest_request_ids()
    # print parser.counter.db_adapter.get_percentil()
    # parser.create_output_file()

if __name__ == "__main__":
    main(sys.argv[1:])

